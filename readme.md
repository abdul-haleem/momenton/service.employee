Build Project
=============
It is a maven project. From command prompt go to service.employee folder and run following command:<br>
````
mvn clean package
````

Run Project
===========
The project has embedded tomcat in it. After building the project run following command:
````    
mvn tomcat7:run
````

It will start the application on port 9090



Endpoints
=========
The application has following exposed endpoints

POST http://localhost:9090/employees
------------------------------------
Creates a new employee.

Example JSON body

{
    "id": 400,
    "name": "Steve",
    "managerId": 150
}

OR for employee without manager

{
    "id": 400,
    "name": "Steve"
}

GET http://localhost:9090/employees
-----------------------------------
Returns all employees.

__Query parameter:__ is-manager.<br>
Optional query parameter. If not provided it returns all employees, otherwise returns according to provided value (true/false).
 

GET http://localhost:9090/employee-no-manager
---------------------------------------------
Fetches the employees in a hierarchical order

Notes
============
* Default data script is executed at application startup. Refer to src/main/resources/schema.xml file.
* To start with empty data, remove insert script from schema.xml and restart application.