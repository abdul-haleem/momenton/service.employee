package com.momenton.dao;

import com.momenton.AppConfig;
import com.momenton.entity.EmployeeEntity;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.context.web.WebAppConfiguration;
import org.springframework.transaction.annotation.Transactional;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;

import java.util.HashSet;
import java.util.List;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertTrue;

@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(classes = {AppConfig.class})
@WebAppConfiguration
public class EmployeeDaoImplTest {

    @PersistenceContext
    private EntityManager em;

    @Autowired
    private EmployeeDao dao;

    @Before
    @Transactional
    public void setup() {
        assertNotNull(em);
        assertNotNull(dao);

        Query query = em.createNativeQuery("DELETE FROM employee WHERE 1=1");
        query.executeUpdate();
    }

    @Test
    @Transactional
    public void testDAOOperations() {

        // Verify database is empty
        List<EmployeeEntity> allEmployees = dao.getAllEmployees();
        assertNotNull(allEmployees);
        assertTrue(allEmployees.isEmpty());

        // Insert one record and verify
        EmployeeEntity entity = new EmployeeEntity();
        entity.setId(100);
        entity.setName("Adam");
        dao.createNew(entity);

        allEmployees = dao.getAllEmployees();
        assertEquals(1, allEmployees.size());

        EmployeeEntity e = dao.findById(100);
        assertNotNull(e);

        // Insert another record
        entity = new EmployeeEntity();
        entity.setId(101);
        entity.setName("Dan");
        entity.setManager(e);
        e.setWorkers(new HashSet<>());
        e.getWorkers().add(entity);
        dao.createNew(entity);

        // Verify size
        allEmployees = dao.getAllEmployees();
        assertEquals(2, allEmployees.size());
        assertEquals(1, dao.countEmployeesWithOutManager());
        assertEquals(1, dao.getAllEmployees(true).size());

    }
}