package com.momenton.service;

import com.momenton.AppConfig;
import com.momenton.model.Employee;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.context.web.WebAppConfiguration;
import org.springframework.transaction.annotation.Transactional;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;

import static org.junit.Assert.*;

@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(classes = {AppConfig.class})
@WebAppConfiguration
public class EmployeeServiceTest {

    @PersistenceContext
    private EntityManager em;

    @Autowired
    EmployeeService service;

    @Before
    @Transactional
    public void setUp() throws Exception {
        assertNotNull(em);
        assertNotNull(service);

        em.createNativeQuery("DELETE FROM employee WHERE 1=1").executeUpdate();

        // Create 2 employees
        Employee e = new Employee();
        e.setId(100);
        e.setName("Adam");
        service.createEmployee(e);

        e = new Employee();
        e.setId(101);
        e.setName("Dan");
        e.setManagerId(100);
        service.createEmployee(e);
    }

    @Test
    @Transactional
    public void getEmployeeById() {
        Employee e = service.getEmployeeById(101);
        assertNotNull(e);
        assertEquals(101, e.getId());
    }


    @Test
    @Transactional
    public void getEmployeeWithOutManager() {
        Employee e = service.getEmployeeWithOutManager();
        assertNotNull(e);
        assertEquals(100, e.getId());
    }

    @Test
    @Transactional
    public void getAllEmployees() {
        assertEquals(2, service.getAllEmployees(null).size());
        assertEquals(1, service.getAllEmployees(false).size());
        assertEquals(1, service.getAllEmployees(true).size());
    }
}