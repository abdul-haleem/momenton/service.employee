package com.momenton.controller;

import com.momenton.model.Employee;
import com.momenton.service.EmployeeService;
import org.junit.Before;
import org.junit.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;
import org.springframework.http.MediaType;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;

import java.util.ArrayList;
import java.util.List;

import static org.hamcrest.CoreMatchers.containsString;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.when;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.result.MockMvcResultHandlers.print;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.content;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;


public class EmployeeControllerTest {

    private MockMvc mockMvc;

    @Mock
    private EmployeeService service;

    @InjectMocks
    private EmployeeController controller;

    @Before
    public void setUp() {
        MockitoAnnotations.initMocks(this);
        mockMvc = MockMvcBuilders.standaloneSetup(controller).build();
    }

    @Test
    public void createEmployee() throws Exception {
        Employee emp = new Employee();
        emp.setId(100);
        emp.setName("Adam");

        when(service.createEmployee(any(Employee.class))).thenReturn(emp);

        mockMvc.perform(post("/employees")
                .contentType(MediaType.APPLICATION_JSON)
                .content("{\"id\":100, \"name\":\"Adam\"}"))
                .andExpect(status().is2xxSuccessful())
                .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
                .andExpect(content().string(containsString("100")))
                .andExpect(content().string(containsString("Adam")));

    }

    @Test
    public void getAllEmployees() throws Exception {
        List<Employee> employees = new ArrayList<>();

        Employee emp = new Employee();
        emp.setId(100);
        emp.setName("Adam");
        employees.add(emp);

        emp = new Employee();
        emp.setId(101);
        emp.setName("Dan");
        emp.setManagerId(100);
        employees.add(emp);


        when(service.getAllEmployees(null)).thenReturn(employees);

        mockMvc.perform(get("/employees"))
                .andDo(print())
                .andExpect(status().is2xxSuccessful())
                .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
                .andExpect(content().string(containsString("100")))
                .andExpect(content().string(containsString("Adam")))
                .andExpect(content().string(containsString("101")))
                .andExpect(content().string(containsString("Dan")));
    }

    @Test
    public void getEmployeeWithoutManager() throws Exception {


        Employee emp = new Employee();
        emp.setId(113);
        emp.setName("Luci");


        when(service.getEmployeeWithOutManager()).thenReturn(emp);

        mockMvc.perform(get("/employee-no-manager"))
                .andDo(print())
                .andExpect(status().is2xxSuccessful())
                .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
                .andExpect(content().string(containsString("113")))
                .andExpect(content().string(containsString("Luci")));

    }
}