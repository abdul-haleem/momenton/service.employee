package com.momenton.controller;

import com.momenton.exception.BadRequestException;
import com.momenton.model.Employee;
import com.momenton.service.EmployeeService;
import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Required;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping("/")
@CrossOrigin
public class EmployeeController {

    @Autowired
    private EmployeeService service;

    private Logger logger = Logger.getLogger(EmployeeController.class);

    @RequestMapping(value = "/employees", method = RequestMethod.POST)
    @ResponseBody
    public Employee createEmployee(@RequestBody  Employee employee) throws Exception {
        try {
            return service.createEmployee(employee);
        } catch (Exception e) {
            throw new BadRequestException("Could not create employee", e);
        }
    }

    @RequestMapping(value = "/employees", method = RequestMethod.GET)
    @ResponseBody
    public List<Employee> getAllEmployees(@RequestParam(value = "is-manager", required = false) Boolean isManager) {
        return service.getAllEmployees(isManager);
    }

    @RequestMapping(value = "/employee-no-manager", method = RequestMethod.GET)
    @ResponseBody
    public Employee getEmployeeWithoutManager() {
        return service.getEmployeeWithOutManager();
    }
}
