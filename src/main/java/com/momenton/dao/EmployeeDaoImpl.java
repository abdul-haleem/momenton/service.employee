package com.momenton.dao;

import com.momenton.entity.EmployeeEntity;
import org.apache.log4j.Logger;
import org.springframework.stereotype.Repository;

import javax.persistence.*;
import java.util.List;

@Repository
public class EmployeeDaoImpl implements EmployeeDao {

    @PersistenceContext
    EntityManager em;

    private Logger logger = Logger.getLogger(EmployeeDao.class);

    @Override
    public EmployeeEntity findById(int id) {
        TypedQuery<EmployeeEntity> query = em.createQuery("FROM EmployeeEntity e WHERE e.id = :id", EmployeeEntity.class);
        query.setParameter("id", id);
        EmployeeEntity entity = null;
        try {
            entity = query.getSingleResult();
        } catch (NoResultException e) {
            logger.warn("Employee with id " + id + " not found");
        }
        return entity;
    }

    @Override
    public void createNew(EmployeeEntity employeeEntity) {
        em.persist(employeeEntity);
    }

    @Override
    public long countEmployeesWithOutManager() {
        TypedQuery<Long> query = em.createQuery("SELECT COUNT(*) FROM EmployeeEntity e WHERE e.manager IS NULL", Long.class);
        long count = 0;
        try {
            count = query.getSingleResult();
        } catch (NoResultException e) {
            logger.warn("No employee found without a manager");
        }
        return count;
    }

    @Override
    public EmployeeEntity getEmployeeWithOutManager() {
        TypedQuery<EmployeeEntity> query = em.createQuery("FROM EmployeeEntity e WHERE e.manager IS NULL", EmployeeEntity.class);
        EmployeeEntity entity = null;
        try {
            entity = query.getSingleResult();
        } catch (NoResultException e) {
            logger.warn("No employee found without a manager");
        }
        return entity;
    }

    @Override
    public List<EmployeeEntity> getAllEmployees() {
        TypedQuery<EmployeeEntity> query = em.createQuery("FROM EmployeeEntity e", EmployeeEntity.class);
        return query.getResultList();
    }

    @Override
    public List<EmployeeEntity> getAllEmployees(boolean isManager) {
        String queryString;
        if (isManager) {
            queryString = "SELECT e FROM EmployeeEntity e JOIN e.workers w WHERE e.id = w.manager.id";
        } else {
            queryString = "SELECT e FROM EmployeeEntity e WHERE NOT EXISTS (SELECT ee FROM EmployeeEntity ee WHERE ee.manager.id = e.id)";
        }

        TypedQuery<EmployeeEntity> query = em.createQuery(queryString, EmployeeEntity.class);
        return query.getResultList();

    }
}
