package com.momenton.dao;

import com.momenton.entity.EmployeeEntity;
import org.springframework.stereotype.Repository;

import java.util.List;

public interface EmployeeDao {
    EmployeeEntity findById(int id);
    void createNew(EmployeeEntity employeeEntity);
    long countEmployeesWithOutManager();
    EmployeeEntity getEmployeeWithOutManager();
    List<EmployeeEntity> getAllEmployees();
    List<EmployeeEntity> getAllEmployees(boolean isManager);
}
