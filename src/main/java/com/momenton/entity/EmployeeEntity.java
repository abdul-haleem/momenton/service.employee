package com.momenton.entity;

import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.ToString;

import javax.persistence.*;
import java.util.Set;

@Data
@Entity
@Table(name = "employee")
@EqualsAndHashCode(exclude = {"workers", "manager"})
@ToString(exclude = {"workers", "manager"})
public class EmployeeEntity {
    @Id
    @Column(name = "id")
    @Basic(optional = false)
    private int id;

    @Column(name = "name")
    private String name;

    @OneToMany(targetEntity = EmployeeEntity.class, cascade={CascadeType.ALL})
    @JoinColumn(name = "manager_id", referencedColumnName = "id")
    private Set<EmployeeEntity> workers;

    @ManyToOne
    @JoinColumn(name = "manager_id", referencedColumnName = "id", insertable = false, updatable = false)
    private EmployeeEntity manager;
}
