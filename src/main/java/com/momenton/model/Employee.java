package com.momenton.model;

import lombok.Data;
import lombok.EqualsAndHashCode;

import java.util.List;

@Data
@EqualsAndHashCode(exclude = {"workers"})
public class Employee {
    private int id;
    private String name;
    private Integer managerId;
    private int level;
    private List<Employee> workers;
}
