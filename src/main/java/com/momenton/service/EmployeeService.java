package com.momenton.service;

import com.momenton.dao.EmployeeDao;
import com.momenton.entity.EmployeeEntity;
import com.momenton.exception.BadRequestException;
import com.momenton.model.Employee;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import org.apache.log4j.Logger;


@Service
public class EmployeeService {

    @Autowired
    private EmployeeDao dao;

    private Logger logger = Logger.getLogger(EmployeeService.class);


    @Transactional
    public Employee getEmployeeById(int id) {
        EmployeeEntity entity = dao.findById(id);
        return toModel(entity);
    }

    @Transactional
    public Employee createEmployee(Employee model) throws BadRequestException {
        logger.info("Creating a new employee");
        EmployeeEntity manager = null;
        if (model.getManagerId() != null) {
            manager = dao.findById(model.getManagerId());

            if (manager == null) {
                logger.error("Invalid manager id provided");
                throw new BadRequestException("Invalid manager id provided");
            }
        }

        EmployeeEntity entity = new EmployeeEntity();
        entity.setId(model.getId());
        entity.setName(model.getName());

        if (manager != null) {
            entity.setManager(manager);
            if (manager.getWorkers() == null) {
                manager.setWorkers(new HashSet<>());
            }
            manager.getWorkers().add(entity);
        } else if (dao.countEmployeesWithOutManager() > 0) {
            logger.error("An employee with no manager already exists");
            throw new BadRequestException("An employee with no manager already exists");
        }

        dao.createNew(entity);

        return getEmployeeById(model.getId());
    }

    @Transactional
    public Employee getEmployeeWithOutManager() {
        EmployeeEntity entity = dao.getEmployeeWithOutManager();
        return toModel(entity, 1);
    }

    @Transactional
    public List<Employee> getAllEmployees(Boolean isManager) {
        List<EmployeeEntity> entities = new ArrayList<>();
        if (isManager == null) {
            entities = dao.getAllEmployees();
        } else {
            entities = dao.getAllEmployees(isManager);
        }

        List<Employee> employees = new ArrayList<>();
        entities.forEach(e->employees.add(toModel(e)));

        return employees;
    }

    private Employee toModel(EmployeeEntity entity) {
        if (entity == null) {
            return null;
        }
        Employee employee = new Employee();
        employee.setId(entity.getId());
        employee.setName(entity.getName());

        if (entity.getManager() != null) {
            employee.setManagerId(entity.getManager().getId());
        }
        return employee;
    }
    private Employee toModel(EmployeeEntity entity, int level) {
        if (entity == null) {
            return null;
        }
        Employee employee = toModel(entity);
        employee.setLevel(level);

        if (entity.getWorkers() != null) {
            level++;
            employee.setWorkers(new ArrayList<>());
            for (EmployeeEntity employeeEntity : entity.getWorkers()) {
                employee.getWorkers().add(toModel(employeeEntity, level));
            }
        }

        return employee;
    }


}
